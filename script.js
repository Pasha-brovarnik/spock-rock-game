import { startConfetti, stopConfetti, removeConfetti } from './confetti.js';

const playerScoreElm = getElement('#playerScore');
const playerChoiceElm = getElement('#playerChoice');
const computerScoreElm = getElement('#computerScore');
const computerChoiceElm = getElement('#computerChoice');
const resultText = getElement('#resultText');
const allGameIcons = document.querySelectorAll('.far');

const playerRock = getElement('#playerRock');
const playerPaper = getElement('#playerPaper');
const playerScissors = getElement('#playerScissors');
const playerLizard = getElement('#playerLizard');
const playerSpock = getElement('#playerSpock');

const computerRock = getElement('#computerRock');
const computerPaper = getElement('#computerPaper');
const computerScissors = getElement('#computerScissors');
const computerLizard = getElement('#computerLizard');
const computerSpock = getElement('#computerSpock');

function getElement(selector) {
	return document.querySelector(selector);
}

const choices = {
	rock: { name: 'Rock', defeats: ['scissors', 'lizard'] },
	paper: { name: 'Paper', defeats: ['rock', 'spock'] },
	scissors: { name: 'Scissors', defeats: ['paper', 'lizard'] },
	lizard: { name: 'Lizard', defeats: ['paper', 'spock'] },
	spock: { name: 'Spock', defeats: ['scissors', 'rock'] },
};

let playerScore = 0;
let computerScore = 0;
let computerChoice = '';

// Reset all selected icons
function resetSelected() {
	allGameIcons.forEach((icon) => {
		icon.classList.remove('selected');
	});
	stopConfetti();
	removeConfetti();
}

// Reset game
function resetAll() {
	playerScore = 0;
	computerScore = 0;
	playerChoiceElm.textContent = '';
	playerScoreElm.textContent = playerScore;
	computerChoiceElm.textContent = '';
	computerScoreElm.textContent = computerScore;
	computerChoice = '';
	resultText.textContent = '';
	resetSelected();
}

window.resetAll = resetAll;

// Random computer choice
function computerRandomChoice() {
	const computerChoiceNumber = Math.random();

	if (computerChoiceNumber < 0.2) {
		computerChoice = 'rock';
	} else if (computerChoiceNumber <= 0.4) {
		computerChoice = 'paper';
	} else if (computerChoiceNumber <= 0.6) {
		computerChoice = 'scissors';
	} else if (computerChoiceNumber <= 0.8) {
		computerChoice = 'lizard';
	} else {
		computerChoice = 'spock';
	}
}

// Check result, update scores, update resultText
function UpdateScore(playerChoice) {
	if (playerChoice === computerChoice) {
		resultText.textContent = "It's a tie!";
		return;
	}

	const choice = choices[playerChoice];
	if (choice.defeats.includes(computerChoice)) {
		startConfetti();
		resultText.textContent = 'You won!';
		playerScore++;
		playerScoreElm.textContent = playerScore;
	} else {
		resultText.textContent = 'You lose!';
		computerScore++;
		computerScoreElm.textContent = computerScore;
	}
}

// Call functions to process the turn
function checkResult(playerChoice) {
	resetSelected();
	computerRandomChoice();
	displayComputerChoice();
	UpdateScore(playerChoice);
}

// Player selections and icon style
function select(playerChoice) {
	checkResult(playerChoice);
	switch (playerChoice) {
		case 'rock':
			playerRock.classList.add('selected');
			playerChoiceElm.textContent = ' --- Rock';
			break;

		case 'paper':
			playerPaper.classList.add('selected');
			playerChoiceElm.textContent = ' --- Paper';
			break;

		case 'scissors':
			playerScissors.classList.add('selected');
			playerChoiceElm.textContent = ' --- Scissors';
			break;

		case 'lizard':
			playerLizard.classList.add('selected');
			playerChoiceElm.textContent = ' --- Lizard';
			break;

		case 'spock':
			playerSpock.classList.add('selected');
			playerChoiceElm.textContent = ' --- Spock';
			break;

		default:
			break;
	}
}

window.select = select;

// Computer selections and icon style
function displayComputerChoice() {
	switch (computerChoice) {
		case 'rock':
			computerRock.classList.add('selected');
			computerChoiceElm.textContent = ' --- Rock';
			break;

		case 'paper':
			computerPaper.classList.add('selected');
			computerChoiceElm.textContent = ' --- Paper';
			break;

		case 'scissors':
			computerScissors.classList.add('selected');
			computerChoiceElm.textContent = ' --- Scissors';
			break;

		case 'lizard':
			computerLizard.classList.add('selected');
			computerChoiceElm.textContent = ' --- Lizard';
			break;

		case 'spock':
			computerSpock.classList.add('selected');
			computerChoiceElm.textContent = ' --- Spock';
			break;

		default:
			break;
	}
}

// On game start set initial values
resetAll();
